class CfgPatches
{
	class dzr_fish_control
	{
		requiredAddons[] = {"DZ_Data"};
		units[] = {};
		weapons[] = {};
	};
};

class CfgMods
{
	class dzr_fish_control
	{
		type = "mod";
		author = "DayZRussia.com";
		description = "Sime fishing tweaks";
		dir = "dzr_fish_control";
		name = "Fish Control Light";
		//inputs = "dzr_fish_control/Data/Inputs.xml";
		dependencies[] = {"Core", "Game", "World", "Mission"};
		class defs
		{
			class engineScriptModule
			{
				files[] = {"dzr_fish_control/Common",  "dzr_fish_control/1_Core"};
			};
			class gameLibScriptModule
			{
				files[] = {"dzr_fish_control/Common",  "dzr_fish_control/2_GameLib"};
			};
			class gameScriptModule
			{
				files[] = {"dzr_fish_control/Common",  "dzr_fish_control/3_Game"};
			};
			class worldScriptModule
			{
				files[] = {"dzr_fish_control/Common", "dzr_fish_control/4_World"};
			};
			class missionScriptModule
			{
				files[] = {"dzr_fish_control/Common", "dzr_fish_control/5_Mission"};
			};

		};
	};
};